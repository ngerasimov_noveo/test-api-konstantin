<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Group;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User
 * 
 * @ORM\Entity
 * @UniqueEntity("email")
 * 
 * @ExclusionPolicy("all") 
 */
class User
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     * 
     * @Expose
     * @Groups({"default", "user_full"})
     */
    private $email;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Type("string")
     *
     * @Expose
     * @Groups({"default", "user_full"})
     */
    private $lastName;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * 
     * @Expose
     * @Groups({"default", "user_full"})
     */
    private $firstName;

    /**
     * @var boolean
     * @Assert\NotBlank()
     * @Assert\Type("boolean")
     * 
     * @Expose
     * @Groups({"default", "user_full"})
     */
    private $state;

    /**
     * @var \DateTime 
     * @Assert\DateTime()
     * 
     * @Expose
     * @Groups({"default", "user_full"})
     */
    private $creationDate;

    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @Expose
     * @Groups({"default", "user_full"})
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="Group", inversedBy="users", cascade={"persist"})
     * @ORM\JoinTable(name="group_user")
     */
    public $groups;

    public function __construct() {
        $this->groups = new \Doctrine\Common\Collections\ArrayCollection();
        
        $this->creationDate = new \DateTime();
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set state
     *
     * @param boolean $state
     *
     * @return User
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return boolean
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     *
     * @return User
     */
    public function setCreationDate(\DateTime $creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Add group
     *
     * @param Group $group
     *
     * @return User
     */
    public function addGroup(Group $group = null)
    {
        if (!$this->hasGroup($group)) {
            $this->groups->add($group);
        }

        return $this;
    }
    
    /**
     * Remove group
     *
     * @param Group $group
     *
     * @return User
     */
    public function removeGroup(Group $group = null)
    {
        if ($this->hasGroup($group)) {
            $this->groups->removeElement($group);
        }

        return $this;
    }
    
    /**
     * Remove all groups
     *
     * @return User
     */
    public function removeAllGroups()
    {
        $this->groups = new \Doctrine\Common\Collections\ArrayCollection();

        return $this;
    }
    
    /**
     * Has group
     *
     * @param Group $group
     *
     * @return boolean
     */
    public function hasGroup(Group $group = null)
    {
        return $this->groups->contains($group);
    }

    /**
     * Get groups
     * 
     * @VirtualProperty 
     * @Groups({"user_full"})
     *
     * @return ArrayCollection of Group
     */
    public function getGroups()
    {
        return $this->groups;
    }
}

