<?php

namespace AppBundle\Entity;

use AppBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Group
 * 
 * @ORM\Entity
 * @UniqueEntity("name")
 * 
 * @ExclusionPolicy("all") 
 */
class Group
{
    /**
     * @var string
     * @Assert\NotBlank() 
     * @Assert\Type("string")
     * @Expose
     * @Groups({"default", "group_full"})
     */
    private $name;

    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @Expose
     * @Groups({"default", "group_full"})
     */
    private $id;
    
    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="groups")
     * @ORM\JoinTable(name="group_user")
     */
    private $users;

    public function __construct() {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set name
     *
     * @param string $name
     *
     * @return Group
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Add user
     *
     * @param Group $user
     *
     * @return Group
     */
    public function addUser(User $user = null)
    {
        if (!$this->hasUser($user)) {
            $this->users->add($user);
        }

        return $this;
    }
    
    /**
     * Remove user
     *
     * @param User $user
     *
     * @return Group
     */
    public function removeUser(User $user = null)
    {
        if ($this->hasUser($user)) {
            $this->users->removeElement($user);
        }

        return $this;
    }
    
    /**
     * Remove all users
     *
     * @return Group
     */
    public function removeAllUsers()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();

        return $this;
    }
    
    /**
     * Has user
     *
     * @param User $user
     *
     * @return boolean
     */
    public function hasUser(User $user = null)
    {
        return $this->users->contains($user);
    }

    /**
     * Get users
     *
     * @return ArrayCollection of User
     * 
     * @VirtualProperty 
     * @Groups({"group_full"})
     */
    public function getUsers()
    {
        return $this->users;
    }
}

