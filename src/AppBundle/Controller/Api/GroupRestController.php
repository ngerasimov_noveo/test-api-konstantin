<?php
namespace AppBundle\Controller\Api;

use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\FOSRestController;
use AppBundle\Entity\Group;
use AppBundle\Form\Type\GroupType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use FOS\RestBundle\Controller\Annotations\View as FOSView;

class GroupRestController extends FOSRestController
{
    /**
    * @Route("/groups/")
    * @Method("GET")
    * 
    * @FOSView(serializerGroups={"default", "group_full"})
    */
    public function indexAction(Request $request){
        $groups = $this->getDoctrine()->getRepository('AppBundle:Group')->findAll();
        
        return $groups;
    }
    
    /**
    * @Route("/groups/")
    * @Method("POST")
    */
    public function createAction(Request $request){
        $group = new Group;
        $response = $this->persistAndFlushByRequest($group, $request);
        
        return $response;
    }
    
    /**
    * @Route("/groups/{id}")
    * @ParamConverter("group", class="AppBundle:Group")
    * @Method("PUT")
    */
    public function updateAction(Group $group, Request $request){
        if(!is_object($group)){
            return $this->handleView(new View(array('status' => 'false', 'message' => 'Group not found'), 500));
        }
        $response = $this->persistAndFlushByRequest($group, $request);
        
        return $response;
    }
    
    private function persistAndFlushByRequest(Group $group, Request $request){
        $form = $this->get('form.factory')->createNamed('', GroupType::class, $group);
        
        //$form->handleRequest($request);
        $form->submit($request->request->all());
        if (!$form->isValid()) {
            $errorMsg = array();
            foreach($form->getErrors(true) as $error) {
                $errorMsg[] = array(
                    'type' => 'parameter',
                    'field' => $error->getOrigin()->getName(),
                    'msg' => $error->getMessage()
                );
            }
            
            return $this->handleView(new View(array('status' => 'false', 'message' => $errorMsg), 500));
        }
        
        $group = $form->getData();
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($group);
        $em->flush();
        
        return $this->handleView($this->view(array('status' => true, 'id' => $group->getId()), 201));
    }
}