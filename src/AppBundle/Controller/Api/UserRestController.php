<?php
namespace AppBundle\Controller\Api;

use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\FOSRestController;
use AppBundle\Entity\User;
use AppBundle\Form\Type\UserType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\RestBundle\Controller\Annotations\View as FOSView;

class UserRestController extends FOSRestController
{
    /**
    * @Route("/users/")
    * @Method("GET")
    * @FOSView(serializerGroups={"default", "user_full"})
    */
    public function indexAction(Request $request){
        $users = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();
        
        return $users;
    }
    
    /**
    * @Route("/users/{id}")
    * @ParamConverter("user", class="AppBundle:User")
    * @Method("GET")
    * @FOSView(serializerGroups={"default", "user_full"})
    */
    public function showAction(User $user){
        if(!is_object($user)){
            return $this->handleView(new View(array('status' => 'false', 'message' => 'User not found'), 500));
        }
        
        return $user;
    }    
    
    /**
    * @Route("/users/")
    * @Method("POST")
    */
    public function createAction(Request $request){
        $user = new User();        
        $response = $this->persistAndFlushByRequest($user, $request);
        
        return $response;
    }
    
    /**
    * @Route("/users/{id}")
    * @ParamConverter("user", class="AppBundle:User")
    * @Method("PUT")
    */
    public function updateAction(User $user, Request $request){
        if(!is_object($user)){
            return $this->handleView(new View(array('status' => 'false', 'message' => 'User not found'), 500));
        }        
        $user->removeAllGroups();
        $response = $this->persistAndFlushByRequest($user, $request);
        
        return $response;
    }
    
    private function persistAndFlushByRequest(User $user, Request $request){
        $form = $this->get('form.factory')->createNamed('',UserType::class, $user);
        
        //$form->handleRequest($request);
        $form->submit($request->request->all());
        if (!$form->isValid()) {
            $errorMsg = array();
            foreach($form->getErrors(true) as $error) {
                $errorMsg[] = array(
                    'type' => 'parameter',
                    'field' => $error->getOrigin()->getName(),
                    'msg' => $error->getMessage()
                );
            }
            
            return $this->handleView(new View(array('status' => 'false', 'message' => $errorMsg), 500));
        }
        $groups = $user->getGroups();
        $user->removeAllGroups();
        foreach($groups as $group){
            $newGroup = $this->getDoctrine()->getRepository('AppBundle:Group')->findOneByName($group->getName());
            if(empty($newGroup)){
                $user->addGroup($group);
            }
            else{
                $user->addGroup($newGroup);
            }
        }
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        
        return $this->handleView($this->view(array('status' => true, 'id' => $user->getId()), 201));
    }
}