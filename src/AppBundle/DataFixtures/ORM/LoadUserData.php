<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $groupRepository = $manager->getRepository('AppBundle:Group');
        $groupAdmin = $groupRepository->findOneByName('admin');
        
        $userAdmin = new User();
        $userAdmin->setEmail('admin@admin.admin');
        $userAdmin->setLastName('admin');
        $userAdmin->setFirstName('admin');
        $userAdmin->setState(true);
        $userAdmin->addGroup($groupAdmin);

        $manager->persist($userAdmin);
        $manager->flush();
    }
    
    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 2;
    }
}