<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\User;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

use Symfony\Component\Form\CallbackTransformer;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', EmailType::class);
        $builder->add('last_name', TextType::class);
        $builder->add('first_name', TextType::class);
        $builder->add('state', ChoiceType::class, array(
            'choices'  => array(
                'Active' => true,
                'Not Active' => false,
            )
        ));
        
        /*$builder->add('groups', CollectionType::class, array(
            'required' => false
        ));*/        
        $builder->add('groups', CollectionType::class, array(
            //'property_path' => 'groups',
            'allow_add' => true,
            'allow_delete' => false,
            'entry_type' => GroupType::class,
            //'by_reference' => false
        ));
        
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
            'csrf_protection' => false,
        ));
    }
}