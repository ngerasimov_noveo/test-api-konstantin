<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160704155202 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $table = $schema->createTable('groups');
        $table->addColumn('id', 'integer', array(
            'autoincrement' => true,
            'unsigned' => true,
            "notnull" => true
        ));
        $table->setPrimaryKey(array('id'));
        
        $table->addColumn('name', 'string');
        $table->addUniqueIndex(array("name"));
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $schema->dropTable('groups');
    }
}
