<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160704153537 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        //email, last name, first name, state (active/ non active), creation date
        
        $table = $schema->createTable('users');
        $table->addColumn('id', 'integer', array(
            'autoincrement' => true,
            'unsigned' => true,
            "notnull" => true
        ));
        $table->setPrimaryKey(array('id'));
        
        $table->addColumn('email', 'string');
        $table->addUniqueIndex(array("email"));
        
        $table->addColumn('last_name', 'string');
        $table->addColumn('first_name', 'string');
        $table->addColumn('state', 'boolean');
        $table->addColumn('creation_date', 'string', array(
            'default' => 'NOW()',
        ));
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $schema->dropTable('users');
    }
}
