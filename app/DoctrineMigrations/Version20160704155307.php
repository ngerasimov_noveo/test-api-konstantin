<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160704155307 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $table = $schema->createTable('group_user');
        
        $table->addColumn('user_id', 'integer', array(
            'unsigned' => true,
            "notnull" => true
        ));        
        $table->addForeignKeyConstraint('users', array("user_id"), array("id"), array("onUpdate" => "CASCADE"));
        
        $table->addColumn('group_id', 'integer', array(
            'unsigned' => true,
            "notnull" => true
        ));
        $table->addForeignKeyConstraint('groups', array("group_id"), array("id"), array("onUpdate" => "CASCADE"));
        
        $table->setPrimaryKey(array('user_id', 'group_id'));
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $schema->dropTable('group_user');
    }
}
